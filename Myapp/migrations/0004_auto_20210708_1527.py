# Generated by Django 3.2.5 on 2021-07-08 07:27

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('Myapp', '0003_auto_20210708_1134'),
    ]

    operations = [
        migrations.AlterField(
            model_name='matrix',
            name='count',
            field=models.IntegerField(default=0, max_length=50),
        ),
        migrations.AlterField(
            model_name='matrix',
            name='uuid',
            field=models.UUIDField(default=uuid.UUID('ec3fa51a-dfbd-11eb-9a8e-5c5f676e0b76'), editable=False, primary_key=True, serialize=False),
        ),
    ]
