from django.db import models
import uuid
# Create your models here.


class matrix (models.Model):
    objects=models.Manager()
    uuid = models.UUIDField(primary_key=True,default=uuid.uuid1(),editable=False)
    source =models.CharField( max_length=50,null=False)
    destination =models.CharField(max_length=50,null=False)
    profile = models.JSONField()
    count = models.IntegerField(default=0)


    # profile内具体内容是
    # {
    #     'regionRule':'white'
    #     'rulelist':{
    #       'whitelist:[TCP_222,TCP_156516],
    #       'blacklist':[TCP_1111],
    #       'graylist':[default]
    #     }
    # }
    # models.UniqueConstraint(fields=['source,destination'],name='src-des')
    class Meta:
        unique_together = (("source", "destination"),)
        indexes = [
            models.Index(fields=['count']),
        ]

class FormSrc(models.Model):
    # 放前端生成页面时需要的数据
    objects=models.Manager()
    src = models.CharField(max_length = 50)
class FormDes(models.Model):
    objects=models.Manager()
    des = models.CharField(max_length = 50)
































# 好像用不着
# class strategy (models.Model):
#     rulename=models.CharField(max_length=50)
#     srcip=models.CharField(max_length=50)
#     srcsection=models.CharField(max_length=50)
#     desip=models.CharField(max_length=50)
#     dessection=models.CharField(max_length=50)
#     port=models.CharField(max_length=50)
#     action=models.CharField(max_length=50)
#     time=models.CharField(max_length=50)
#     comment=models.CharField(max_length=50)
#     option=models.CharField(max_length=50)







