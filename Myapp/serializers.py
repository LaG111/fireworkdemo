from rest_framework import serializers
from Myapp.models import matrix
class matrixSerializer(serializers.ModelSerializer):
    class Meta:
        model = matrix
        fields = "__all__"