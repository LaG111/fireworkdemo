import re
import json
from jsonschema import validate

# 检测内网ip合法性，范围为10.x.x.x/172.16.x.x-172.31.x.x/192.168.x.x
def ipIsValid(ip):
    boolean = re.match(r'(10\.(25[0-5]|2[0-4][0-9]|[0-1]?[0-9]?[0-9])|'
                       r'172\.(1[6-9]|2[0-9]|3[0-1]|192\.168)(\.(25[0-5]|2[0-4][0-9]|[0-1]?[0-9]?[0-9])){2}$)',ip)
    if boolean:
        return True
    else:
        return False

schema = {
        "type": "object",
        "required": [
            "ruleProfile",
            "regionRule",
            "Device",
            "ruleList"
        ],
        "maxProperties": 4,
        "minProperties": 4,
        "properties": {
            "ruleProfile": {
                "type": "string"
            },
            "regionRule": {
                "type": "string",
                "pattern": "white|grey|black"
            },
            "Device": {
                "type": "string"
            },
            "ruleList": {
                "type": "object",
                "required": [
                    "whiteList",
                    "greyList",
                    "blackList"
                ],
                "maxProperties": 3,
                "minProperties": 3,
                "properties": {
                    "whiteList": {
                        "type": "array",
                        "items": {
                            "type": "string",
                            "pattern": "^(Default|(TCP|UDP)_\d+)$"
                        }
                    },
                    "greyList": {
                        "type": "array",
                        "items": {
                            "type": "string",
                            "pattern": "^(Default|(TCP|UDP)_\d+)$"
                        }
                    },
                    "blackList": {
                        "type": "array",
                        "items": {
                            "type": "string",
                            "pattern": "^(Default|(TCP|UDP)_\d+)$"
                        }
                    }
                }
            }
        }
    }

# 使用jsonschema来检测json格式是否正确
def jsonIsValid(jsonData):
    print( type(jsonData))
    if(type(jsonData)==str):
        jsonData=json.loads(jsonData)

    validate(jsonData,schema)
    return True


# 根据数据库中的json返回区域是否可通行，不可通返回2，需审批返回1，可通行返回0
def check(jsonData, Protocol, Port):
    try:
        s = json.loads(jsonData)
    except:
        print("Loading Json Error")
    else:
        try:
            jsonIsValid(s)
        except:
            print("Json Data Error")
        else:
            regionColour = s.get('regionRule')
            if regionColour == 'black':
                print("不可通")
                return 2
            ruleList = s.get('ruleList')
            Fport = Protocol + '_' + str(Port)
            if regionColour == 'grey':
                for i in ruleList['blackList']:
                    if i == Fport or i == 'Default':
                        print("不可通")
                        return 2
                print("需审批")
                return 1
            elif regionColour == 'white':
                for i in ruleList['blackList']:
                    if i == Fport or i == 'Default':
                        print("不可通")
                        return 2
                for i in ruleList['greyList']:
                    if i == Fport or i == 'Default':
                        print("需审批")
                        return 1
                print("可通行")
                return 0


# jsonData = '{"ruleProfile":"fsdfsdf","regionRule":"white","Device":"1","ruleList":{"whiteList":["Default"],"greyList":["TCP_3333","UDP_888"],"blackList":["TCP_3358","TCP_3359"]}}'
# check(jsonData, 'TCP', 3359)
# check(jsonData, 'TCP', 8080)
# check(jsonData, 'UDP', 1069)
# check(jsonData, 'TCP', 3333)
# check(jsonData, 'UDP', 888)
# # s=json.loads(jsonData)
# jsonIsValid(jsonData)
# ip1 = "192.168.12.256"
# ip2 = "172.31.234.125"
# if ipisvalid(ip1):
#     print("Valid ip")
# else:
#     print("Invalid ip")
# if ipisvalid(ip2):
#     print("Valid ip")
# else:
#     print("Invalid ip")











