from django.shortcuts import render

# Create your views here.
import json
from django.shortcuts import render, redirect
from Myapp.models import  matrix,FormSrc,FormDes
from django.http.response import JsonResponse,HttpResponse
from rest_framework.parsers import JSONParser
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import mixins
from rest_framework import generics
from django.views import View
from django.core import serializers
from rest_framework.decorators import api_view
import Myapp.util as util
# util里放后台哥做的逻辑判断函数


from Myapp.serializers import matrixSerializer

class MatrixView(APIView):
    def get(self,request,format=None):
        """
            【功能描述】matrix的查  <br />
            【需要参数】source，destination  <br />
            【返回参数】  <br />
        """
        source=request.GET.get('source')
        destination=request.GET.get('destination')
        if (source == None or destination == None):
            return Response(Result().formIsNotFilled())
        print(source,destination)

        try:
            data = matrix.objects.filter(source=str(source), destination=str(destination))
            if data:
                count = data.first().count + 1
                data.update(count=count)
                serializer = matrixSerializer(data.first())
                return Response(Result().success(data=serializer.data))
            else:
                return Response(Result.ruleNotExist())
        except Exception as e:
            print(e)
            return Response(Result().unkErr())

    def post(self,request,format=None):
        """
                【功能描述】matrix增  <br />
                【需要参数】source，destination，profile，  <br />
                【返回参数】formdata={
                'source':source,
                'destination':destination,
            }  <br />
            """

        # request里携带了添加的数据

        source=request.POST.get('source')
        destination=request.POST.get('destination')
        profile=request.POST.get('profile')
        print(source,destination,profile)
        if(source==None or destination==None or profile==None):
            return Response(Result().formIsNotFilled())
        elif (not util.jsonIsValid(profile)):
            return Response(Result().proIsNotValid())
        elif (matrix.objects.filter(source=source,destination=destination).first() != None):
            return Response(data=Result().customized(code='500',message="该条目已经存在"))

        try:
            matrix.objects.create(source=source, destination=destination, profile=profile)
        except Exception as e:
            print(e)
            return Response(data=Result().unkErr())
        else:
            return Response(data=Result().success(data='created'))


    def delete(self,request,format=None):
        """
                【功能描述】matrix的删  <br />
                【需要参数】source，destination  <br />
                【返回参数】
            }  <br />
        """
        source=request.data.get('source')
        destination=request.data.get('destination')
        if (source == None or destination == None):
            return Response(data=Result().formIsNotFilled())
        print(source,destination)
        try:
            col=matrix.objects.filter(source=source,destination=destination)
            if(col.first()==None ):
                return Response(Result().ruleNotExist())
            else:
                col.delete()
        except Exception as e:
            print(e)
            return Response(data=Result().unkErr())
        else:
            return Response(data=Result().success())

    def put(self,request,format=None):
        """
                【功能描述】matrix的改  <br />
                【需要参数】source，destination，profile  <br />
                【返回参数】  <br />
            """
        source = request.data.get('source')
        destination = request.data.get('destination')
        profile=request.data.get('profile')
        print(source,destination,profile)
        if (source == None or destination == None or profile == None):
            return Response(data=Result().formIsNotFilled())
        if (not util.jsonIsValid(profile)):
            return Response(data=Result().proIsNotValid())
        operation=matrix.objects.filter(source=source,destination=destination)
        # serializer= matrixSerializer(data=request.data)

        if operation.first():
            operation.update(profile=profile)
            return Response(data=Result().success(message='updated'),status=status.HTTP_200_OK)
        else:
            operation.update(profile=profile)
            return Response(data=Result().ruleNotExist())




@api_view(['GET'])
def form(request,format=None):
    """
        【功能描述】用于获取生成表单所需的source，destination数据  <br />
        【需要参数】啥都不需要  <br />
        【返回参数】
{
    "source": [
        "sec1",
        "sec2",
        "sec3",
        "sec4"
    ],
    "destination": [
        "sec1",
        "sec2",
        "sec3",
        "sec4"
    ]
}
    """

    try:
        source=FormSrc.objects.all()
        destination=FormDes.objects.all()
    except Exception as e:
        print(e)
        return Response(data=Result().unkErr())
    print(source,destination)
    src=[]
    des=[]
    for i in source:
        src.append(i.src)
    for i in destination:
        des.append(i.des)
    formdata={
        'source':src,
        'destination':des,
    }
    print(formdata)
    return Response(data=Result().success(data=formdata))


@api_view(['POST'])
def verification(request,format=None):
    """
        【功能描述】用于验证所提交的表单的要求能不能被通过  <br />
        【需要参数】source,destination,Protocol,Port  <br />
        【返回参数】可以放行/需要审核/不允许  <br />
    """

    source=request.data.get('source')
    destination=request.data.get('destination')
    if (source == None or destination == None ):
        return Response(data=Result().formIsNotFilled())
    protocol=request.data.get('protocol')
    port=request.data.get('port')
    profile=getProfile(source,destination)
    print('profile')
    print(profile)
    print(protocol,port)
    if(source==None or destination==None or protocol==None or port==None):
        return Response(Result().formIsNotFilled())
    elif(profile==None):
        return Response(Result().ruleNotExist())
    try:
        verification=util.check(jsonData=profile, Protocol=protocol, Port=port)
        # 2不可通 1需审批 0可通行
        if (verification==2):
            return Response(data=Result().success(data='可以通行'))
        elif(verification==1):
            return Response(data=Result().success(data='需要审批'))
        elif(verification==0):
            return Response(data=Result().success(data='不可通行'))
        else:
            return Response(data=Result().ruleNotExist())
    except Exception as e:
        print(e)
        return Response(Result().unkErr())



def getProfile(source, destination):
    try:
        data = matrix.objects.filter(source=source, destination=destination)
        count = data.first().count + 1
        data.update(count=count)
        profile = data.first().profile
    except Exception as e:
        print(e)
        return Response(data=Result().unkErr())
    return profile

#
# def SDIsValid(S,D):
#     try:
#         if(FormSrc.objects.filter(src=S).first()==None or FormDes.objects.filter(des=D).first()==None):
#             return False
#         else:
#             return True
#     except Exception as e:
#         print(e)
#         print('未知错误，可能是没连上数据库')


class Result:
    def setCode(self,code):
        self.code=code
    def setMessage(self,message):
        self.message=message
    def setData(self,data):
        self.data=data
    def serialized(self):
        dic={
            'code':self.code,
            'data':self.data,
            'message':self.message,
        }
        return dic
    def unkErr(self):
        dic={
            'code':'500',
            'data':'',
            'message':'未知错误'
        }
        return dic

    def proIsNotValid(self):
        dic={
            'code':'500',
            'data':'',
            'message':"profile不合法"
        }
        return dic
    def formIsNotFilled(self):
        dic={
            'code':'500',
            'data':'',
            'message':"表单数据不完整"
        }
        return dic
    def ruleNotExist(self):
        dic = {
            'code': '500',
            'data': '',
            'message': "该条目不存在"
        }
        return dic
    def success(self,data='',message='success'):

        dic={
            'code':'200',
            'data':data,
            'message':message
        }
        return dic
    def customized(self,code='',data='',message=''):
        dic={
            'code':code,
            'data':data,
            'message':message
        }
        return dic





